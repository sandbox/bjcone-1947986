<?php


function commerce_pmm_admin_form($form, &$form_state) {
  $form = array();
  // Lets start with minimum quantity required for each product.
  // After we get that working, we can move on to maximum quantity.
  
  
  $form['#tree'] = TRUE;
  
  $product_types = commerce_product_types();
  $instance = array();
  $enabled = array();
  foreach ($product_types as $type => $type_info) {
    $form[$type] = array(
      '#type' => 'fieldset',
      '#title' => t('@name (@type)', array('@name' => $type_info['name'], '@type' => $type_info['type'])),
      '#description' => t('Configure Min and Max settings for @type', array('@type'=>$type_info['name'])),
      '#collapsible' => TRUE,
    );
    foreach (_commerce_pmm_field_names() as $field_info) { 
      $instance[$type][$field_info['name']] = field_info_instance('commerce_product', $field_info['name'], $type);
      $enabled[$type][$field_info['name']] = (!empty($instance[$type][$field_info['name']]));
      
      $form[$type][$field_info['name']] = array(
        '#type' => 'checkbox',
        '#title' => t('@field',array('@field' =>$field_info['title'])),
        '#description' => t('Enable @field restrictions @product', 
          array('@field' => $field_info['title'], '@product' => $type_info['name'])),
        '#default_value' => $enabled[$type][$field_info['name']],
      );
    }
  }
  $form['submit'] = array(
    '#value' => t('Save Configuration'),
    '#type' => 'submit'
  );
  
  // Last items (potential for separate module) will be min/max purchase price.

  return $form;
}

function commerce_pmm_admin_form_submit(&$form, &$form_state) {
  
  foreach (commerce_product_types() as $type => $type_info) {
    foreach (_commerce_pmm_field_names() as $field_name => $field_info) {
      $instance = field_info_instance('commerce_product', $field_info['name'], $type);
      $currently_enabled = (!empty($instance));
      $newly_enabled = $form_state['values'][$type][$field_info['name']];
      
      if ($newly_enabled && !$currently_enabled) {
        commerce_pmm_create_instance($field_info['name'], 'number_integer', TRUE, 'commerce_product', $type, 
          t('@title',array('@title' => $field_info['title'])));
        drupal_set_message(t('@field created for @product.', 
          array('@field'=>$field_info['title'], '@product' => $type)));
      }
      
      elseif (!$newly_enabled && $currently_enabled) {
        field_delete_instance($instance);
        drupal_set_message(t('@field disabled for @product.', 
          array('@field' => $field_info['title'], '@product' => $type_info['name'])));
      }
    }
  }
}

function _commerce_pmm_field_names() {
  return array(
    'commerce_min_quantity' => array(
      'name' => 'commerce_min_quantity',
      'title' => 'Minimum Quantity',
    ),
    'commerce_max_quantity' => array(
      'name' => 'commerce_max_quantity',
      'title' => 'Maximum Quantity',
    ),
    'commerce_min_price' => array(
      'name' => 'commerce_min_price',
      'title' => 'Minimum Purchase Price',
    ),
    'commerce_max_price' => array(
      'name' => 'commerce_max_price',
      'title' => 'Maximum Purchase Price',
    ),
  );
}